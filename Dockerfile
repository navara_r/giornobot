FROM python:3.8-slim-buster

WORKDIR /app

RUN python3 -m pip install -U discord.py && python -m pip install python-dotenv

COPY . .

CMD [ "python3", "bot.py"]
